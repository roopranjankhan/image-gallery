// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: 'AIzaSyCn_uF6WMrsOQlAjzeC7iz4JcNmCgtnMT4',
    authDomain: 'image-gallery-b41db.firebaseapp.com',
    databaseURL: 'https://image-gallery-b41db-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'image-gallery-b41db',
    storageBucket: 'image-gallery-b41db.appspot.com',
    messagingSenderId: '391656342710',
    appId: '1:391656342710:web:55fd6e23631bfeec9f1217'
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
